# feedback-widget

a frontend feedback widget sending the feedback messages to a backend env and storing into a db

## deploy

currently deployed at: [Feedback Widget by Murilera](https://feedback-widget-murilera.vercel.app/)

- vercel
- railapp

## front-end

- typescript
- react
- vite
- phosphor-react
- headless ui
- tailwindcss
- html2canvas

## back-end

- nodejs
- typescript
- express
- prisma
- nodemailer

## principles

- SOLID

## third-party

- mailtrap

## db

- sqlite (development)
- postgresql (production)

## tests

- jest
- swc
